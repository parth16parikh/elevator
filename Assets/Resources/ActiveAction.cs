﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActiveAction : MonoBehaviour
{
    //[SerializeField]
    public GameObject actionFont;

    public void StartActionFontAnimation()
    {
        actionFont.SetActive(true);

    }

}
