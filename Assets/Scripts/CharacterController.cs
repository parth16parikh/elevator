﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    private enum CharacterState
    {
        Idle,
        Walking,
        Jumping
    }

    private enum CharacterDirection
    {
        Left,
        Right
    }

    [SerializeField]
    private Animator m_Animator;
    [SerializeField]
    private GameObject m_Player;
    [SerializeField]
    private LayerMask m_FloorMask;
    [SerializeField]
    private GameObject m_FireHandWalking,m_FireHandJumping;
    [SerializeField]
    private Transform m_BulletOrigin;
    [SerializeField]
    private GameObject m_BulletPrefab;

    private Rigidbody2D m_RigidBody;
    
    private CharacterState m_CharacterState = CharacterState.Idle;
    private CharacterDirection m_CharacterDirection = CharacterDirection.Right;
    private Vector3 m_InitialScale;
    private bool m_IsGrounded = true;
    private bool m_JumPStarted = false;
    private Bullet[] m_Bullets;

    private void OnEnable()
    {
        EventManager.StartListening(GameEventType.FireBullet, OnEventReceived);
        EventManager.StartListening(GameEventType.GoLeft, OnEventReceived);
        EventManager.StartListening(GameEventType.GoLeftUp, OnEventReceived);
        EventManager.StartListening(GameEventType.GoRight, OnEventReceived);
        EventManager.StartListening(GameEventType.GoRightUp, OnEventReceived);
        EventManager.StartListening(GameEventType.Jump, OnEventReceived);
    }

    private void OnDisable()
    {
        EventManager.StopListning(GameEventType.FireBullet, OnEventReceived);
        EventManager.StopListning(GameEventType.GoLeft, OnEventReceived);
        EventManager.StopListning(GameEventType.GoLeftUp, OnEventReceived);
        EventManager.StopListning(GameEventType.GoRight, OnEventReceived);
        EventManager.StopListning(GameEventType.GoRightUp, OnEventReceived);
        EventManager.StopListning(GameEventType.Jump, OnEventReceived);
    }

    private void Awake()
    {
        m_Bullets = new Bullet[Constants.BULLET_COUNT];
        m_RigidBody = GetComponent<Rigidbody2D>();
        m_InitialScale = m_Player.transform.localScale;
        for(int i = 0; i < Constants.BULLET_COUNT; i++)
        {
            Bullet l_bullet = Instantiate<GameObject>(m_BulletPrefab, m_BulletOrigin, true).GetComponent<Bullet>();
            l_bullet.transform.localPosition = Vector3.zero;
            l_bullet.gameObject.SetActive(false);
            m_Bullets[i] = l_bullet;
        }
    }

    private void OnEventReceived(GameEventType gameEventType)
    {
        switch(gameEventType)
        {
            case GameEventType.FireBullet:
                CheckAndFireBullet();
                break;

            case GameEventType.GoLeft:
                KeepWalking(Vector3.left);
                break;

            case GameEventType.GoLeftUp:
                StopWalking(Vector3.left);
                break;

            case GameEventType.GoRight:
                KeepWalking(Vector3.right);
                break;

            case GameEventType.GoRightUp:
                StopWalking(Vector3.right);
                break;

            case GameEventType.Jump:
                if (m_IsGrounded)
                {
                    GoToState(CharacterState.Jumping);
                }
                break;
        }
    }

    private void GoToState(CharacterState characterState)
    {
        if(m_CharacterState == characterState)
        {
            return;
        }
        switch(characterState)
        {
            case CharacterState.Idle:
                m_Animator.SetTrigger(Constants.CHARACTER_START_IDLE);
                break;

            case CharacterState.Walking:
                m_Animator.SetTrigger(Constants.CHARACTER_START_WALKING);
                break;

            case CharacterState.Jumping:
                StartJump();
                break;
        }
        m_CharacterState = characterState;
    }


    void CheckAndFireBullet()
    {
        for(int i = 0; i < m_Bullets.Length; i++)
        {
            if (!m_Bullets[i].InUse)
            {
                m_Bullets[i].InUse = true;
                StartCoroutine(FireBullet(m_Bullets[i]));
                return;
            }
        }
    }

    IEnumerator FireBullet(Bullet bullet)
    {
        if (m_IsGrounded)
        {
            m_FireHandWalking.SetActive(true);
        }
        else
        {
            m_FireHandJumping.SetActive(true);
        }

        bullet.Fire();

        yield return new WaitForSeconds(0.1f);
        m_FireHandWalking.SetActive(false);
        m_FireHandJumping.SetActive(false);
    }

    private void StartJump()
    {
        m_IsGrounded = false;
        m_JumPStarted = true;
        m_Animator.SetTrigger(Constants.CHARACTER_START_JUMP);
        m_RigidBody.velocity += Vector2.up * Constants.JUMP_FORCE;
    }

    private void KeepWalking(Vector3 direction)
    {
        if(direction == Vector3.left)
        {
            m_CharacterDirection = CharacterDirection.Left;
            m_Player.transform.localScale = new Vector3(m_InitialScale.x * -1.0f, m_InitialScale.y, m_InitialScale.z);
        }
        else if(direction == Vector3.right)
        {
            m_Player.transform.localScale = m_InitialScale;
            m_CharacterDirection = CharacterDirection.Right;
        }
        if (m_IsGrounded)
        {
            GoToState(CharacterState.Walking);
        }
        m_RigidBody.velocity = new Vector2(direction.x, m_RigidBody.velocity.y);
    }

    public void JumpCompleted()
    {
        if (m_CharacterState == CharacterState.Jumping)
        {
            GoToState(CharacterState.Idle);
        }
        else if(m_CharacterState == CharacterState.Walking)
        {
            GoToState(CharacterState.Walking);
        }

    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (m_JumPStarted)
        {
            return;
        }
        if (((1 << collision.gameObject.layer) & m_FloorMask) != 0)
        {
            Debug.Log("on collision Enter");
            //It matched one
            m_IsGrounded = true;
        }
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (((1 << collision.gameObject.layer) & m_FloorMask) != 0)
        {
            //It matched one
            Debug.Log("on collision exit");
            m_IsGrounded = false;
            m_JumPStarted = false;
        }
    }

    private void StopWalking(Vector3 direction)
    {
        GoToState(CharacterState.Idle);
        if (m_IsGrounded)
        {
            m_RigidBody.velocity = Vector3.zero;
        }
        
    }
}
