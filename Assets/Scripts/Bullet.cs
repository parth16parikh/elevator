﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField]
    private GameObject m_Blast;

    private Rigidbody2D m_RigidBody;
    private bool m_InUse = false;
    public bool InUse
    {
        get
        {
            return m_InUse;
        }

        set
        {
            m_InUse = value;
        }
    }

    void Awake()
    {
      m_RigidBody = GetComponent<Rigidbody2D>();
    }

    public void Fire()
    {
        gameObject.SetActive(true);
        m_RigidBody.velocity = Vector2.right * 2.0f;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        m_Blast.SetActive(true);
        m_RigidBody.velocity = Vector2.zero;
        StartCoroutine(ResetBullet());
    }

    IEnumerator ResetBullet()
    {
        yield return new WaitForSeconds(0.05f);
        InUse = false;
        transform.localPosition = Vector3.zero;
        m_Blast.SetActive(false);
        gameObject.SetActive(false);
    }
}
