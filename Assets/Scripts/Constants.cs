﻿public static class Constants
{
    public static string CHARACTER_START_WALKING = "StartWalking";
    public static string CHARACTER_START_IDLE = "StartIdle";
    public static string CHARACTER_START_JUMP = "StartJump";

    public static float JUMP_FORCE = 3.0f;

    public static int BULLET_COUNT = 3;
}
