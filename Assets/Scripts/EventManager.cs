﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
public enum GameEventType
{
    GoLeft,
    GoLeftUp,
    GoRight,
    GoRightUp,
    FireBullet,
    Jump
}

public class EventManager : MonoBehaviour
{
    private Dictionary<GameEventType, Action<GameEventType>> m_EventDictionary;

    private static EventManager m_EventManager;

    private static EventManager Instance
    {
        get
        {
            if(m_EventManager == null)
            {
                m_EventManager = FindObjectOfType<EventManager>();
                if(m_EventManager == null)
                {
                    Debug.LogError("There needs to be one active EventManger script on a GameObject in your scene.");
                }
                else
                {
                    m_EventManager.Init();
                }
            }
            return m_EventManager;
        }
    }


    void Init()
    {
        if(m_EventManager != null)
        {
            m_EventDictionary = new Dictionary<GameEventType, Action<GameEventType>>();
        }
    }


    public static void StartListening(GameEventType gameEventType, Action<GameEventType> action)
    {
        Action<GameEventType> l_Action;
        if(Instance.m_EventDictionary.TryGetValue(gameEventType, out l_Action))
        {
            l_Action += action;
            Instance.m_EventDictionary[gameEventType] = action;
        }
        else
        {
            l_Action += action;
            Instance.m_EventDictionary.Add(gameEventType, action);
        }
    }

    public static void StopListning(GameEventType gameEventType, Action<GameEventType> action)
    {
        if(m_EventManager == null)
        {
            return;
        }
        Action<GameEventType> l_Action;
        if (Instance.m_EventDictionary.TryGetValue(gameEventType, out l_Action))
        {
            l_Action -= action;
            Instance.m_EventDictionary[gameEventType] = l_Action;
        }
    }

    public static void TriggerEvent(GameEventType gameEventType)
    {
        Action<GameEventType> l_Action;

        if(Instance.m_EventDictionary.TryGetValue(gameEventType, out l_Action))
        {
            l_Action.Invoke(gameEventType);
        }
    }
}
