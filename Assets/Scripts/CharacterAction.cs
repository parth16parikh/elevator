﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterAction : MonoBehaviour
{
    private CharacterController m_CharacterController;

    private void Awake()
    {
        m_CharacterController = GetComponentInParent<CharacterController>();
    }
    
    public void JumpAnimationCompleted()
    {
        m_CharacterController.JumpCompleted();
    }
}
