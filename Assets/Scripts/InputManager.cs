﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {
            //Go left pressed
            EventManager.TriggerEvent(GameEventType.GoLeft);
        }
        else if (Input.GetKeyUp(KeyCode.A))
        {
            //Go Left lifted
            EventManager.TriggerEvent(GameEventType.GoLeftUp);
        }
        else if (Input.GetKey(KeyCode.D))
        {
            //Go Right pressed
            EventManager.TriggerEvent(GameEventType.GoRight);
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            //Go Right Lifted
            EventManager.TriggerEvent(GameEventType.GoRightUp);
        }

        if (Input.GetKeyDown(KeyCode.W))
        {
            //Go elevator up
        }

        if (Input.GetKeyDown(KeyCode.D))
        {
            //Go elevator down or duck
        }
        else if (Input.GetKeyUp(KeyCode.D))
        {
            //Stop Duck
        }

        if(Input.GetKeyUp(KeyCode.Space))
        {
            //Jump
            EventManager.TriggerEvent(GameEventType.Jump);
        }

        if(Input.GetKeyUp(KeyCode.E))
        {
            EventManager.TriggerEvent(GameEventType.FireBullet);
        }
    }
}
